package com.example.onlyfishes.ui.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.ActivityAddNewCatchBinding
import com.example.onlyfishes.models.CaughtFish
import com.example.onlyfishes.utils.ConnectionManager
import com.example.onlyfishes.utils.DateFormater
import com.example.onlyfishes.viewmodels.AddNewCatchActivityViewModel
import java.util.*


class AddNewCatchActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddNewCatchBinding
    private val viewModel by viewModels<AddNewCatchActivityViewModel>()
    private val SELECT_IMAGE_REQUEST_CODE = 1
    private var imageFilePath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddNewCatchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = "Add new catch"

        binding.apply {
            btnPickImage.setOnClickListener {
                selectImage()
            }
            btnUpload.setOnClickListener {
                if (ConnectionManager.isConnectionAvailable()) {
                    upload()
                } else Toast.makeText(
                    this@AddNewCatchActivity,
                    R.string.noConnectionMessage,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        viewModel.toast.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
        viewModel.uploadProgress.observe(this, {
            val value = it.toInt()
            binding.progressbar.progress = value
            if (value == 100) {
                binding.progressbar.visibility = View.GONE
            }
        })
        viewModel.finishActivity.observe(this, {
            if (it) {
                this.finish()
            }
        })
    }

    private fun selectImage() {
        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, SELECT_IMAGE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED && requestCode == SELECT_IMAGE_REQUEST_CODE && data != null) {
            binding.ivPickedImage.setImageURI(data.data)
            binding.ivPickedImage.visibility = View.VISIBLE
            imageFilePath = data.data.toString()
        }
    }

    private fun upload() {
        binding.apply {
            val species = etSpecies.text.toString().trim()
            val mass = etMass.text.toString()
            val length = etLength.text.toString()
            val date = Date(datepicker.year - 1900, datepicker.month, datepicker.dayOfMonth)
            val formatedDate = DateFormater.format(date)
            if (species.isNotEmpty() and mass.isNotEmpty() and length.isNotEmpty() and imageFilePath.isNotEmpty() and (date <= Date())) {
                binding.progressbar.visibility = View.VISIBLE
                val id = System.currentTimeMillis()
                viewModel.upload(
                    CaughtFish(
                        id,
                        species,
                        mass.toDouble(),
                        length.toDouble(),
                        formatedDate,
                        "${id}.${viewModel.getFileExtension(Uri.parse(imageFilePath))}"
                    ), imageFilePath
                )
            } else Toast.makeText(
                this@AddNewCatchActivity,
                "Some of the fields are empty or invalid",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}