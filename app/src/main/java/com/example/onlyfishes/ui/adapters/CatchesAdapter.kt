package com.example.onlyfishes.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.onlyfishes.CatchItemInteractionListener
import com.example.onlyfishes.databinding.CaughtfishHolderBinding
import com.example.onlyfishes.models.CaughtFish
import com.example.onlyfishes.utils.DateFormater

class CatchesAdapter(private val listener: CatchItemInteractionListener) :
    RecyclerView.Adapter<CatchViewHolder>() {

    private val caughtFishes = mutableListOf<CaughtFish>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CatchViewHolder(
        CaughtfishHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: CatchViewHolder, position: Int) =
        holder.bind(caughtFishes[position], listener)

    override fun getItemCount() = caughtFishes.size

    fun getCatch(position: Int) = caughtFishes[position]

    fun addAll(caughtFishes: MutableList<CaughtFish>) {
        this.caughtFishes.clear()
        this.caughtFishes.addAll(caughtFishes)
        this.notifyDataSetChanged()
    }

    fun remove(position: Int) {
        this.caughtFishes.removeAt(position)
        this.notifyItemRemoved(position)
    }
}

class CatchViewHolder(private val binding: CaughtfishHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {


    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun bind(catch: CaughtFish, listener: CatchItemInteractionListener) {
        binding.apply {
            tvSpecies.text = catch.species
            tvMass.text = "Mass: ${catch.mass} kg"
            tvDate.text = "Date: ${catch.dateOfCatch}"

            root.setOnClickListener {
                listener.onCatchClick(adapterPosition)
            }
        }
    }

}