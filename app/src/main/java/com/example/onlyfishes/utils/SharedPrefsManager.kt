package com.example.onlyfishes.utils

import android.content.Context
import com.google.android.gms.maps.GoogleMap

object SharedPrefsManager {
    private const val SHARED_PREFS_KEY = "ONLYFISHES_SHAREDPREFS_KEY"
    private const val DARK_MODE_KEY = "DARK_MODE_KEY"
    private const val MAP_TYPE_KEY = "MAP_TYPE_KEY"


    fun saveDarkModeEnabled(context: Context, enabled: Boolean) {
        context.getSharedPreferences(SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit()
            .putBoolean(DARK_MODE_KEY, enabled).apply()
    }

    fun isDarkModeEnabled(context: Context) =
        context.getSharedPreferences(SHARED_PREFS_KEY, Context.MODE_PRIVATE)
            .getBoolean(DARK_MODE_KEY, false)

    fun saveMapType(context: Context, type: Int) {
        context.getSharedPreferences(SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit()
            .putInt(MAP_TYPE_KEY, type).apply()
    }

    fun getMapType(context: Context) =
        context.getSharedPreferences(SHARED_PREFS_KEY, Context.MODE_PRIVATE)
            .getInt(MAP_TYPE_KEY, GoogleMap.MAP_TYPE_NORMAL)
}