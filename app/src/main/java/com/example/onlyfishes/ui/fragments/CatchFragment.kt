package com.example.onlyfishes.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.onlyfishes.OnlyFishesApp
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.FragmentCatchBinding
import com.example.onlyfishes.utils.ConnectionManager
import com.example.onlyfishes.viewmodels.CatchFragmentViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.*


class CatchFragment : Fragment() {
    lateinit var binding: FragmentCatchBinding
    private val viewModel by viewModel<CatchFragmentViewModel>()
    private val args: CatchFragmentArgs by navArgs()
    private var bitmap: Bitmap? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCatchBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val catch = args.catch
        binding.apply {
            progressbar.visibility = View.VISIBLE
            tvSpecies.text = catch.species
            tvMass.text = "Mass: ${catch.mass} kg"
            tvLength.text = "Length: ${catch.length} cm"
            tvDateOfCatch.text = "Date caught: ${catch.dateOfCatch}"
            rbtnDefaultShareText.setOnClickListener {
                tilShareText.visibility = View.GONE
            }
            rbtnCustomText.setOnClickListener {
                tilShareText.visibility = View.VISIBLE
            }
            btnShare.setOnClickListener {
                val text =
                    if (binding.rbtnCustomText.isChecked) binding.etShareText.text.toString() else viewModel.getDefaultShareText(
                        catch
                    )
                if (bitmap != null) {
                    share(this@CatchFragment.bitmap!!, text)
                    Toast.makeText(
                        activity,
                        "Wait for the image to prepare for sharing",
                        Toast.LENGTH_SHORT
                    ).show()
                } else Toast.makeText(activity, "Image did not loaded yet", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        viewModel.fetchCatchImg(catch.imgName)
        viewModel.ivCatchImgBitmap.observe(viewLifecycleOwner, {
            this.bitmap = it
            binding.ivCatchImg.setImageBitmap(it)
            binding.progressbar.visibility = View.GONE
        })
        if (!ConnectionManager.isConnectionAvailable()) {
            Toast.makeText(activity, R.string.noConnectionMessage, Toast.LENGTH_LONG).show()
        }
    }


    private fun share(bitmap: Bitmap, text: String) {
        binding.btnShare.isEnabled = false
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            //---Save bitmap to external cache directory---//
            //get cache directory
            val cachePath = File(activity?.externalCacheDir, "my_images/")
            cachePath.mkdirs()

            //create png file
            val file = File(cachePath, "imageToShare.png")
            val fileOutputStream: FileOutputStream
            try {
                fileOutputStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
                fileOutputStream.flush()
                fileOutputStream.close()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }


            //create a intent
            withContext(Dispatchers.Main) {
                //---Share File---//
                //get file uri
                val myImageFileUri = FileProvider.getUriForFile(
                    OnlyFishesApp.context,
                    activity?.applicationContext?.packageName + ".provider",
                    file
                )

                val intent = Intent(Intent.ACTION_SEND).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    putExtra(Intent.EXTRA_TEXT, text)
                    putExtra(Intent.EXTRA_STREAM, myImageFileUri)
                    type = "image/*"
                }
                startActivity(Intent.createChooser(intent, "Share via:"))
                binding.btnShare.isEnabled = true
            }
        }
    }

}