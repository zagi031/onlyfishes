package com.example.onlyfishes.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlyfishes.models.CaughtFish
import com.example.onlyfishes.utils.DateFormater
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class AnalyticsFragmentViewModel : ViewModel() {
    private val currentUserID = FirebaseAuth.getInstance().currentUser?.uid
    private val databaseRef =
        Firebase.database.reference.child("uploads").child(currentUserID.toString())
    private val _numOfCatches = MutableLiveData<Int>()
    val numOfCatches: LiveData<Int> = _numOfCatches
    private val _heaviestCatch = MutableLiveData<CaughtFish>()
    val heaviestCatch: LiveData<CaughtFish> = _heaviestCatch
    private val _longestCatch = MutableLiveData<CaughtFish>()
    val longestCatch: LiveData<CaughtFish> = _longestCatch
    private val _latestCatch = MutableLiveData<CaughtFish>()
    val latestCatch: LiveData<CaughtFish> = _latestCatch
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    init {
        fetchCatches()
    }

    private fun fetchCatches() {
        databaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val catches = mutableListOf<CaughtFish>()
                snapshot.children.forEach {
                    val caughtFish = it.getValue(CaughtFish::class.java)!!
                    catches.add(caughtFish)
                }
                _numOfCatches.postValue(catches.size)
                getHeaviestCatch(catches)
                getLongestCatch(catches)
                getLatestCatch(catches)
            }

            override fun onCancelled(error: DatabaseError) {
                _toast.postValue(error.message)
            }
        })
    }

    private fun getHeaviestCatch(catches: List<CaughtFish>) {
        _heaviestCatch.postValue(catches.maxByOrNull { it.mass })
    }

    private fun getLongestCatch(catches: List<CaughtFish>) =
        _longestCatch.postValue(catches.maxByOrNull { it.length })

    private fun getLatestCatch(catches: List<CaughtFish>) =
        _latestCatch.postValue(catches.maxByOrNull { DateFormater.format(it.dateOfCatch) })

    fun getUserDisplayName() = FirebaseAuth.getInstance().currentUser?.displayName

}