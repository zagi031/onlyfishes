package com.example.onlyfishes.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.onlyfishes.utils.DarkModeManager
import com.example.onlyfishes.utils.SharedPrefsManager
import com.example.onlyfishes.viewmodels.SplashScreenActivityViewModel

class SplashScreenActivity : AppCompatActivity() {
    private val viewModel by viewModels<SplashScreenActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        DarkModeManager.setTheme(SharedPrefsManager.isDarkModeEnabled(this))
        super.onCreate(savedInstanceState)

        if (viewModel.isUserLoggedIn()) {
            startActivity(Intent(this, HomeActivity::class.java))
        } else startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}