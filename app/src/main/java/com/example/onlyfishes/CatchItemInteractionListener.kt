package com.example.onlyfishes

interface CatchItemInteractionListener {
    fun onCatchClick(position: Int)
}