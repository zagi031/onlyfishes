package com.example.onlyfishes.viewmodels

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class HomeActivityViewModel: ViewModel() {
    private val mAuth = FirebaseAuth.getInstance()

    fun logOut() = mAuth.signOut()
}