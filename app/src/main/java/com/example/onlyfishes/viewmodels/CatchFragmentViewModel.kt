package com.example.onlyfishes.viewmodels

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlyfishes.models.CaughtFish
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage

class CatchFragmentViewModel : ViewModel() {
    private val currentUserID = FirebaseAuth.getInstance().currentUser?.uid
    private val storageRef =
        FirebaseStorage.getInstance().getReference("uploads").child(currentUserID.toString())
    private val _ivCatchImgBitmap = MutableLiveData<Bitmap>()
    val ivCatchImgBitmap: LiveData<Bitmap> = _ivCatchImgBitmap

    companion object {
        private const val maxDownloadSizeBytes: Long = 1024 * 1024 * 10   // 10 MB
    }

    fun fetchCatchImg(imgName: String) {
        val ref = storageRef.child(imgName)
        ref.getBytes(maxDownloadSizeBytes).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
            _ivCatchImgBitmap.postValue(bitmap)
        }
    }

    fun getDefaultShareText(caughtFish: CaughtFish) =
        "I caught ${caughtFish.species} on ${caughtFish.dateOfCatch}. It weighs ${caughtFish.mass} kg and its length is ${caughtFish.length} cm."

}