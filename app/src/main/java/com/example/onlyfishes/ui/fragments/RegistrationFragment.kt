package com.example.onlyfishes.ui.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.onlyfishes.R
import com.example.onlyfishes.viewmodels.RegistrationFragmentViewModel
import com.example.onlyfishes.databinding.FragmentRegistrationBinding
import com.example.onlyfishes.ui.activities.HomeActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegistrationFragment : Fragment() {

    private lateinit var binding: FragmentRegistrationBinding
    private val viewModel by viewModel<RegistrationFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegistrationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            viewModel.toast.observe(viewLifecycleOwner, {
                Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
            })
            viewModel.navigateToHomeActivity.observe(viewLifecycleOwner, {
                startActivity(Intent(activity, HomeActivity::class.java))
                activity?.finish()
            })
            tvShowLoginFragment.setOnClickListener {
                Navigation.findNavController(view)
                    .navigate(R.id.action_registrationFragment_to_loginFragment)
            }
            btnRegister.setOnClickListener {
                clearErrors()
                val email = etEmail.text.toString()
                val emailIsValid = viewModel.validateEmail(email)
                if (!emailIsValid) {
                    tilEmail.error = "Enter valid email"
                }

                val password = etPassword.text.toString()
                val passwordIsValid = viewModel.validatePassword(password)
                if (!passwordIsValid) {
                    tilPassword.error = "Password too weak"
                }

                val passwordMatch = etPassword.text.toString() == etRepeatedPassword.text.toString()
                if (!passwordMatch) {
                    tilRepeatedPassword.error = "Input password don't match"
                }

                val displayName = etDisplayName.text.toString()
                val displayNameNotEmpty = displayName.trim().isNotEmpty()
                if (!displayNameNotEmpty) {
                    tilDisplayName.error = "Must not be empty"
                }

                if (emailIsValid && passwordIsValid && passwordMatch && displayNameNotEmpty) {
                    viewModel.register(email, password, displayName)
                }
            }
        }
    }

    private fun clearErrors() {
        binding.apply {
            tilEmail.error = null
            tilDisplayName.error = null
            tilPassword.error = null
            tilRepeatedPassword.error = null
        }
    }
}