package com.example.onlyfishes.viewmodels

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class SplashScreenActivityViewModel : ViewModel() {
    fun isUserLoggedIn() = FirebaseAuth.getInstance().currentUser != null
}