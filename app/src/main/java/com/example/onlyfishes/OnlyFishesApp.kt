package com.example.onlyfishes

import android.app.Application
import android.content.Context
import com.example.onlyfishes.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class OnlyFishesApp : Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this

        startKoin {
            androidContext(this@OnlyFishesApp)
            modules(viewModelModule)
        }
    }
}