package com.example.onlyfishes.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DateFormater {
    private const val pattern = "dd. MMMM yyyy."

    @SuppressLint("SimpleDateFormat")
    fun format(date: Date): String = SimpleDateFormat(pattern).format(date)

    @SuppressLint("SimpleDateFormat")
    fun format(string: String) = SimpleDateFormat(pattern).parse(string)
}