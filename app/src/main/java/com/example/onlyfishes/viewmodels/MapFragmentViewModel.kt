package com.example.onlyfishes.viewmodels

import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlyfishes.OnlyFishesApp
import com.example.onlyfishes.R
import com.example.onlyfishes.models.FishingSpot
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MapFragmentViewModel : ViewModel() {
    private val currentUserID = FirebaseAuth.getInstance().currentUser?.uid
    private val databaseRef =
        Firebase.database.reference.child("savedFishingSpots").child(currentUserID.toString())
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast
    private val _fishingSpots = MutableLiveData<List<FishingSpot>>()
    val fishingSpots: LiveData<List<FishingSpot>> = _fishingSpots

    private lateinit var soundPool: SoundPool
    private var soundIsLoaded: Boolean = false
    private var soundID: Int = -1

    fun loadSound() {
        soundPool = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SoundPool.Builder().setMaxStreams(1).build()
        } else {
            SoundPool(1, AudioManager.STREAM_MUSIC, 0)
        }
        soundPool.setOnLoadCompleteListener { _, _, _ -> soundIsLoaded = true }
        soundID = soundPool.load(OnlyFishesApp.context, R.raw.hit, 1)
    }

    fun playSound() = soundPool.play(soundID, 1f, 1f, 1, 0, 1f)

    fun saveFishingSpot(marker: Marker) {
        val fishingSpot = FishingSpot(
            System.currentTimeMillis(),
            marker.position.latitude,
            marker.position.longitude,
            marker.title!!
        )
        databaseRef.child(fishingSpot.id.toString()).setValue(fishingSpot)
    }

    fun fetchSavedFishingSpots() {
        databaseRef.get().addOnSuccessListener { it ->
            val savedFishingSpots = mutableListOf<FishingSpot>()
            it.children.forEach {
                val fishingSpot = it.getValue(FishingSpot::class.java)!!
                savedFishingSpots.add(fishingSpot)
            }
            _fishingSpots.postValue(savedFishingSpots)
        }
    }

    fun deleteFishingSpot(position: LatLng) {
        databaseRef.get().addOnSuccessListener { it ->
            var id: Long = 0
            it.children.forEach {
                val fishingSpot = it.getValue(FishingSpot::class.java)!!
                if (fishingSpot.lat == position.latitude && fishingSpot.long == position.longitude) {
                    id = fishingSpot.id
                }
            }
            databaseRef.child(id.toString()).removeValue()
        }
    }

}