package com.example.onlyfishes.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.FragmentLoginBinding
import com.example.onlyfishes.ui.activities.HomeActivity
import com.example.onlyfishes.viewmodels.LoginFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    lateinit var binding: FragmentLoginBinding
    private val viewModel by viewModel<LoginFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            viewModel.toast.observe(viewLifecycleOwner, {
                Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
            })
            viewModel.navigateToHomeActivity.observe(viewLifecycleOwner, {
                startActivity(Intent(activity, HomeActivity::class.java))
                activity?.finish()
            })
            tvShowRegistrationFragment.setOnClickListener {
                Navigation.findNavController(view)
                    .navigate(R.id.action_loginFragment_to_registrationFragment)
            }
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                if (email.isNotEmpty() && password.isNotEmpty()) {
                    viewModel.login(email, password)
                } else Toast.makeText(activity, "Some of inputs are empty", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}