package com.example.onlyfishes.viewmodels

import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlyfishes.OnlyFishesApp
import com.example.onlyfishes.models.CaughtFish
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage

class AddNewCatchActivityViewModel : ViewModel() {
    private val currentUserID = FirebaseAuth.getInstance().currentUser?.uid
    private val storageRef =
        FirebaseStorage.getInstance().getReference("uploads").child(currentUserID.toString())
    private val databaseRef =
        Firebase.database.reference.child("uploads").child(currentUserID.toString())
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast
    private val _finishActivity = MutableLiveData<Boolean>()
    val finishActivity: LiveData<Boolean> = _finishActivity
    private val _uploadProgress = MutableLiveData<Double>()
    val uploadProgress: LiveData<Double> = _uploadProgress

    fun getFileExtension(uri: Uri): String {
        val cR = OnlyFishesApp.context.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri)).toString()
    }

    fun upload(caughtFish: CaughtFish, imageFilePath: String) {
        val fileRef = storageRef.child(caughtFish.imgName)
        fileRef.putFile(Uri.parse(imageFilePath)).addOnSuccessListener {
            databaseRef.child(caughtFish.id.toString()).setValue(caughtFish).addOnCompleteListener {
                _toast.postValue("Uploaded successfully")
                _finishActivity.postValue(true)
            }.addOnFailureListener {
                _toast.postValue(it.message)
            }
        }.addOnFailureListener {
            _toast.postValue(it.message)
        }.addOnProgressListener {
            val progress = 100.0 * it.bytesTransferred / it.totalByteCount
            _uploadProgress.postValue(progress)
        }
    }
}