package com.example.onlyfishes.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlyfishes.utils.LoginValidator
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class RegistrationFragmentViewModel : ViewModel() {
    private val inputValidator = LoginValidator
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast
    private val _navigateToHomeActivity = MutableLiveData<Boolean>()
    val navigateToHomeActivity: LiveData<Boolean> = _navigateToHomeActivity
    private val mAuth = FirebaseAuth.getInstance()

    fun validateEmail(email: CharSequence) = inputValidator.isValidEmail(email)
    fun validatePassword(password: CharSequence) = inputValidator.isValidPassword(password)

    fun register(email: String, password: String, displayName: String) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val user = mAuth.currentUser
                val profileUpdates =
                    UserProfileChangeRequest.Builder().setDisplayName(displayName).build()
                user?.updateProfile(profileUpdates)?.addOnCompleteListener {
                    _toast.postValue("Welcome ${mAuth.currentUser?.displayName}")
                    _navigateToHomeActivity.postValue(true)
                }
            } else {
                _toast.postValue(task.exception?.message.toString())
            }
        }
    }
}