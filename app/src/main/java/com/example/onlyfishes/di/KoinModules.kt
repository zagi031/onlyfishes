package com.example.onlyfishes.di

import com.example.onlyfishes.viewmodels.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { RegistrationFragmentViewModel() }
    viewModel { LoginFragmentViewModel() }
    viewModel { SplashScreenActivityViewModel() }
    viewModel { AddNewCatchActivityViewModel() }
    viewModel { CatchesFragmentViewModel() }
    viewModel { CatchFragmentViewModel() }
    viewModel { HomeActivityViewModel() }
    viewModel { MapFragmentViewModel() }
    viewModel { AnalyticsFragmentViewModel() }
}