package com.example.onlyfishes.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.ActivityHomeBinding
import com.example.onlyfishes.utils.DarkModeManager
import com.example.onlyfishes.utils.SharedPrefsManager
import com.example.onlyfishes.viewmodels.HomeActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private val viewModel by viewModel<HomeActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bottomNavigationView.setBackgroundColor(resources.getColor(R.color.navbarColor))

        val navController = findNavController(R.id.fragment_homeactivity)
        val appBarConfiguration =
            AppBarConfiguration(setOf(R.id.catchesFragment, R.id.mapFragment, R.id.catchFragment, R.id.analyticsFragment))
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.bottomNavigationView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_bar, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.getItem(0)?.isChecked = SharedPrefsManager.isDarkModeEnabled(this)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_logOut -> {
                AlertDialog.Builder(this).setTitle("Alert")
                    .setMessage("Are you sure you want to log out?")
                    .setPositiveButton("Yes") { _, _ ->
                        viewModel.logOut()
                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    }
                    .setNegativeButton("No") { _, _ -> }.create().show()
            }
            R.id.item_darkMode -> {
                item.isChecked = !item.isChecked
                DarkModeManager.setTheme(item.isChecked)
                SharedPrefsManager.saveDarkModeEnabled(this, item.isChecked)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}