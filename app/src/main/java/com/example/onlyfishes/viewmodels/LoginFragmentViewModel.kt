package com.example.onlyfishes.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth

class LoginFragmentViewModel : ViewModel() {
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast
    private val _navigateToHomeActivity = MutableLiveData<Boolean>()
    val navigateToHomeActivity: LiveData<Boolean> = _navigateToHomeActivity
    private val mAuth = FirebaseAuth.getInstance()

    fun login(email: String, password: String) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                _toast.postValue("Welcome ${mAuth.currentUser?.displayName}")
                _navigateToHomeActivity.postValue(true)
            } else {
                _toast.postValue(task.exception?.message.toString())
            }
        }
    }
}