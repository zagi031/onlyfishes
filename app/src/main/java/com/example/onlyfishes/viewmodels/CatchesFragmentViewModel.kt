package com.example.onlyfishes.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlyfishes.models.CaughtFish
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage

class CatchesFragmentViewModel : ViewModel() {
    private val currentUserID = FirebaseAuth.getInstance().currentUser?.uid
    private val storageRef =
        FirebaseStorage.getInstance().getReference("uploads").child(currentUserID.toString())
    private val databaseRef =
        Firebase.database.reference.child("uploads").child(currentUserID.toString())
    private val _catches = MutableLiveData<MutableList<CaughtFish>>()
    val catches: LiveData<MutableList<CaughtFish>> = _catches
    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String> = _toast

    init {
        databaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val catches = mutableListOf<CaughtFish>()
                snapshot.children.forEach {
                    val caughtFish = it.getValue(CaughtFish::class.java)!!
                    catches.add(caughtFish)
                }
                _catches.postValue(catches)
            }

            override fun onCancelled(error: DatabaseError) {
                _toast.postValue(error.message)
            }

        })
    }

    fun deleteCatch(caughtFish: CaughtFish) {
        databaseRef.child(caughtFish.id.toString()).removeValue()
        storageRef.child(caughtFish.imgName).delete()
    }
}