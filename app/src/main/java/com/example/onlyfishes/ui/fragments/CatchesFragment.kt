package com.example.onlyfishes.ui.fragments

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Dimension
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.onlyfishes.CatchItemInteractionListener
import com.example.onlyfishes.OnlyFishesApp
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.FragmentCatchesBinding
import com.example.onlyfishes.ui.activities.AddNewCatchActivity
import com.example.onlyfishes.ui.adapters.CatchesAdapter
import com.example.onlyfishes.utils.ConnectionManager
import com.example.onlyfishes.viewmodels.CatchesFragmentViewModel
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.concurrent.thread


class CatchesFragment : Fragment(), CatchItemInteractionListener {

    lateinit var binding: FragmentCatchesBinding
    private val adapter = CatchesAdapter(this)
    private val viewModel by viewModel<CatchesFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCatchesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        binding.apply {
            progressbar.visibility = View.VISIBLE
            fbtnAddCatch.visibility = View.VISIBLE
            YoYo.with(Techniques.BounceInUp).playOn(fbtnAddCatch)
            fbtnAddCatch.setOnClickListener {
                startActivity(Intent(activity, AddNewCatchActivity::class.java))
            }
        }
        viewModel.catches.observe(viewLifecycleOwner, {
            adapter.addAll(it)
            binding.progressbar.visibility = View.GONE
        })
        viewModel.toast.observe(viewLifecycleOwner, {
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        })
        if (!ConnectionManager.isConnectionAvailable()) {
            Toast.makeText(activity, R.string.noConnectionMessage, Toast.LENGTH_LONG).show()
        }
    }

    private fun setupRecyclerView() {
        binding.apply {
            recyclerview.layoutManager =
                LinearLayoutManager(this@CatchesFragment.context, RecyclerView.VERTICAL, false)
            recyclerview.adapter = this@CatchesFragment.adapter
        }

        val simpleItemTouchCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                AlertDialog.Builder(context)
                    .setTitle("Warning")
                    .setMessage("Really want to delete this caught fish from list?")
                    .setPositiveButton(
                        "Yes"
                    ) { _, _ ->
                        val position = viewHolder.adapterPosition
                        val catch = adapter.getCatch(position)
                        adapter.remove(position)
                        thread(start = true) {
                            Thread.sleep(500)
                            viewModel.deleteCatch(catch)
                        }
                    }
                    .setNegativeButton(
                        "No"
                    ) { _, _ -> adapter.notifyItemChanged(viewHolder.adapterPosition) }
                    .setOnCancelListener {
                        adapter.notifyItemChanged(viewHolder.adapterPosition)
                    }.create().show()
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {

                RecyclerViewSwipeDecorator.Builder(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                    .addSwipeLeftBackgroundColor(
                        ContextCompat.getColor(
                            OnlyFishesApp.context,
                            R.color.onRecylcerItemSwipeColor
                        )
                    )
                    .addSwipeLeftActionIcon(R.drawable.ic_trashcan)
                    .addSwipeRightBackgroundColor(
                        ContextCompat.getColor(
                            OnlyFishesApp.context,
                            R.color.onRecylcerItemSwipeColor
                        )
                    )
                    .addSwipeRightActionIcon(R.drawable.ic_trashcan)
                    .setIconHorizontalMargin(Dimension.DP, 0)
                    .create()
                    .decorate()

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }
        ItemTouchHelper(simpleItemTouchCallback).attachToRecyclerView(binding.recyclerview)
    }

    override fun onCatchClick(position: Int) {
        val catch = adapter.getCatch(position)
        val action = CatchesFragmentDirections.actionCatchesFragmentToCatchFragment(catch)
        Navigation.findNavController(binding.root).navigate(action)
    }
}