package com.example.onlyfishes.models

import java.io.Serializable

class CaughtFish() : Serializable {
    var id: Long = 0
    var species: String = ""
    var mass: Double = 0.0
    var length: Double = 0.0
    var dateOfCatch: String = ""
    var imgName: String = ""

    constructor(
        id: Long,
        species: String,
        mass: Double,
        length: Double,
        dateOfCatch: String,
        imgName: String
    ) : this() {
        this.id = id
        this.species = species
        this.mass = mass
        this.length = length
        this.dateOfCatch = dateOfCatch
        this.imgName = imgName
    }

    override fun toString(): String {
        return "$species of $mass kg and $length cm"
    }
}