package com.example.onlyfishes.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.FragmentAnalyticsBinding
import com.example.onlyfishes.utils.ConnectionManager
import com.example.onlyfishes.viewmodels.AnalyticsFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AnalyticsFragment : Fragment() {

    private lateinit var binding: FragmentAnalyticsBinding
    private val viewModel by viewModel<AnalyticsFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAnalyticsBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressbar.visibility = View.VISIBLE
        binding.tvTitle.text = "${viewModel.getUserDisplayName()}'s stats"
        viewModel.numOfCatches.observe(viewLifecycleOwner, {
            if (it > 0) {
                showUI()
            }
            binding.tvNumberOfCatches.text = it.toString()
            binding.progressbar.visibility = View.GONE
        })
        viewModel.heaviestCatch.observe(viewLifecycleOwner, {
            if (it != null) {
                binding.tvHeaviestFish.text = it.toString()
            }
        })
        viewModel.longestCatch.observe(viewLifecycleOwner, {
            if (it != null) {
                binding.apply {
                    tvLongestFish.text = it.toString()
                }
            }
        })
        viewModel.latestCatch.observe(viewLifecycleOwner, {
            if (it != null) {
                binding.apply {
                    tvLatestFish.text = it.toString()
                }
            }
        })
        if (!ConnectionManager.isConnectionAvailable()) {
            Toast.makeText(activity, R.string.noConnectionMessage, Toast.LENGTH_LONG).show()
        }

    }

    private fun showUI() {
        binding.apply {
            tvHeaviestFishLabel.visibility = View.VISIBLE
            tvHeaviestFish.visibility = View.VISIBLE
            tvLongestFishLabel.visibility = View.VISIBLE
            tvLongestFish.visibility = View.VISIBLE
            tvLatestFishLabel.visibility = View.VISIBLE
            tvLatestFish.visibility = View.VISIBLE
        }
    }
}