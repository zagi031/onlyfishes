package com.example.onlyfishes.ui.fragments

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.onlyfishes.OnlyFishesApp
import com.example.onlyfishes.R
import com.example.onlyfishes.databinding.FragmentMapBinding
import com.example.onlyfishes.models.FishingSpot
import com.example.onlyfishes.utils.ConnectionManager
import com.example.onlyfishes.utils.SharedPrefsManager
import com.example.onlyfishes.viewmodels.MapFragmentViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.SettingsDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapFragment : Fragment(), EasyPermissions.PermissionCallbacks {

    private lateinit var binding: FragmentMapBinding
    private val viewModel by viewModel<MapFragmentViewModel>()
    private var markerCurrentLocation: Marker? = null
    private var mMap: GoogleMap? = null
    private val locationRequestCode = 10
    private val locationListener = object : LocationListener {
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
        override fun onLocationChanged(location: Location) {
            updateLocation(location)
        }
    }
    private var extendFloatingButtonsMenu = false
    private val enterFromBottom: Animation by lazy {
        AnimationUtils.loadAnimation(
            OnlyFishesApp.context,
            R.anim.enter_from_bottom
        )
    }
    private val exitToBottom: Animation by lazy {
        AnimationUtils.loadAnimation(
            OnlyFishesApp.context,
            R.anim.exit_to_bottom
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        viewModel.loadSound()
        viewModel.toast.observe(viewLifecycleOwner, {
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        })
        binding.apply {
            fbtnShowMapTypeMenu.setOnClickListener {
                setFloatingButtonsAnimations(extendFloatingButtonsMenu)
                setFloatingButtonsVisibility(extendFloatingButtonsMenu)
                extendFloatingButtonsMenu = !extendFloatingButtonsMenu
            }
            fbtnSaveCurrentLocation.setOnClickListener {
                if (markerCurrentLocation != null) {
                    saveFishingSpot(
                        markerCurrentLocation!!.position.latitude,
                        markerCurrentLocation!!.position.longitude
                    )
                } else Toast.makeText(
                    activity,
                    "Current location is still not fetched",
                    Toast.LENGTH_SHORT
                ).show()
            }
            fbtnNormalType.setOnClickListener {
                this@MapFragment.mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                SharedPrefsManager.saveMapType(
                    requireActivity().baseContext,
                    GoogleMap.MAP_TYPE_NORMAL
                )
            }
            fbtnSatelliteType.setOnClickListener {
                this@MapFragment.mMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
                SharedPrefsManager.saveMapType(
                    requireActivity().baseContext,
                    GoogleMap.MAP_TYPE_SATELLITE
                )
            }
            fbtnTerrainType.setOnClickListener {
                this@MapFragment.mMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
                SharedPrefsManager.saveMapType(
                    requireActivity().baseContext,
                    GoogleMap.MAP_TYPE_TERRAIN
                )
            }
        }
        if (!ConnectionManager.isConnectionAvailable()) {
            Toast.makeText(activity, R.string.noConnectionMessage, Toast.LENGTH_LONG).show()
        }
    }

    private fun setFloatingButtonsVisibility(clicked: Boolean) {
        if (!clicked) {
            binding.apply {
                fbtnSaveCurrentLocation.visibility = View.VISIBLE
                fbtnNormalType.visibility = View.VISIBLE
                fbtnSatelliteType.visibility = View.VISIBLE
                fbtnTerrainType.visibility = View.VISIBLE
                fbtnSaveCurrentLocation.isClickable = true
                fbtnNormalType.isClickable = true
                fbtnSatelliteType.isClickable = true
                fbtnTerrainType.isClickable = true
            }
        } else {
            binding.apply {
                fbtnSaveCurrentLocation.visibility = View.GONE
                fbtnNormalType.visibility = View.GONE
                fbtnSatelliteType.visibility = View.GONE
                fbtnTerrainType.visibility = View.GONE
                fbtnSaveCurrentLocation.isClickable = false
                fbtnNormalType.isClickable = false
                fbtnSatelliteType.isClickable = false
                fbtnTerrainType.isClickable = false
            }
        }
    }

    private fun setFloatingButtonsAnimations(clicked: Boolean) {
        if (!clicked) {
            binding.fbtnSaveCurrentLocation.startAnimation(enterFromBottom)
            binding.fbtnNormalType.startAnimation(enterFromBottom)
            binding.fbtnSatelliteType.startAnimation(enterFromBottom)
            binding.fbtnTerrainType.startAnimation(enterFromBottom)
        } else {
            binding.fbtnSaveCurrentLocation.startAnimation(exitToBottom)
            binding.fbtnNormalType.startAnimation(exitToBottom)
            binding.fbtnSatelliteType.startAnimation(exitToBottom)
            binding.fbtnTerrainType.startAnimation(exitToBottom)
        }
    }

    private val callback = OnMapReadyCallback { googleMap ->
        googleMap.mapType = SharedPrefsManager.getMapType(requireActivity().baseContext)
        binding.apply {
            fbtnShowMapTypeMenu.visibility = View.VISIBLE
            YoYo.with(Techniques.BounceInUp).playOn(fbtnShowMapTypeMenu)
        }
        this.mMap = googleMap
        YoYo.with(Techniques.SlideInUp)
        this.mMap!!.setOnMarkerClickListener {
            if (it != markerCurrentLocation) {
                AlertDialog.Builder(context)
                    .setTitle("Warning")
                    .setMessage("Really want to delete this fishing spot from map?")
                    .setPositiveButton("Yes") { _, _ ->
                        it.remove()
                        viewModel.deleteFishingSpot(it.position)
                    }
                    .setNegativeButton("No") { _, _ -> }
                    .create().show()
            }
            false
        }
        this.mMap!!.setOnMapLongClickListener {
            saveFishingSpot(it.latitude, it.longitude)
        }
        viewModel.fetchSavedFishingSpots()
        viewModel.fishingSpots.observe(viewLifecycleOwner, {
            it.forEach {
                addFishingSpotToMap(it)
            }
        })
        if (hasLocationPermissions()) {
            startTrackingLocation()
        } else requestLocationPermission()
    }

    private fun startTrackingLocation() {
        val locationManager =
            activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        Toast.makeText(activity, "Fetching current location...", Toast.LENGTH_LONG).show()
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE
        val provider = locationManager.getBestProvider(criteria, true)
        val minTime = 1000L
        val minDistance = 10.0F
        try {
            locationManager.requestLocationUpdates(
                provider!!,
                minTime,
                minDistance,
                locationListener
            )
        } catch (e: SecurityException) {
            Toast.makeText(activity, "Permission not granted", Toast.LENGTH_SHORT).show()
        }
    }

    private fun saveFishingSpot(latitude: Double, longitude: Double) {
        if (ConnectionManager.isConnectionAvailable()) {
            viewModel.playSound()
            val fishingSpot = FishingSpot()
            fishingSpot.title = "Fishing spot"
            fishingSpot.lat = latitude
            fishingSpot.long = longitude
            viewModel.saveFishingSpot(addFishingSpotToMap(fishingSpot))
        } else Toast.makeText(activity, R.string.noConnectionMessage, Toast.LENGTH_LONG)
            .show()
    }

    private fun addFishingSpotToMap(fishingSpot: FishingSpot): Marker {
        val latLng = LatLng(fishingSpot.lat, fishingSpot.long)
        val marker =
            MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(createFishingSpotBitmapMarker()))
                .title(fishingSpot.title)
        return mMap?.addMarker(marker)!!
    }

    private fun createFishingSpotBitmapMarker(): Bitmap {
        val bitmapDrawable = ContextCompat.getDrawable(
            requireActivity().baseContext,
            R.drawable.ic_fishmarker
        ) as BitmapDrawable
        val bitmap = bitmapDrawable.bitmap
        return Bitmap.createScaledBitmap(bitmap, 90, 90, false)
    }

    private fun updateLocation(location: Location) {
        val position = LatLng(location.latitude, location.longitude)
        val zoomLevel = 18f
        markerCurrentLocation?.remove()
        markerCurrentLocation =
            mMap?.addMarker(MarkerOptions().position(position).title("Here I Am"))
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(position, zoomLevel))
    }

    private fun hasLocationPermissions() = EasyPermissions.hasPermissions(
        requireContext(),
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private fun requestLocationPermission() {
        EasyPermissions.requestPermissions(
            this,
            "This application cannot track your location without Location Permission",
            locationRequestCode,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(
                this,
                listOf(
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            )
        ) {
            SettingsDialog.Builder(requireActivity()).build().show()
        } else {
            requestLocationPermission()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        startTrackingLocation()
    }

}