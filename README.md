# OnlyFishes

OnlyFishes is an Android fishing app. You can manage your catch record and share your catches (photo+text) via social media, save your favourite fishing spots on Google Maps fragment and track your fishing analytics. To use this app, you need to sign up using built-in signing up form.

To download .apk file, click [here](https://gitlab.com/zagi031/onlyfishes/-/blob/master/onlyfishes.apk).

Technologies used: Android, Kotlin, RecyclerView, Firebase services (Authentication, Storage, Realtime database), Koin, Navigation Component, Google maps
