package com.example.onlyfishes.utils

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate

object DarkModeManager {
    fun setTheme(isDarkModeEnabled: Boolean) {
        if (isDarkModeEnabled) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}