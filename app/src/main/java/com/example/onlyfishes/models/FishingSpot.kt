package com.example.onlyfishes.models

class FishingSpot() {
    var id: Long = 0
    var lat: Double = 0.0
    var long: Double = 0.0
    var title: String = ""

    constructor(id: Long, lat: Double, long: Double, title: String) : this() {
        this.id = id
        this.lat = lat
        this.long = long
        this.title = title
    }
}